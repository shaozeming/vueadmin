﻿import Vue from 'vue'
import Router from 'vue-router'

import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

Vue.use(Router);


const routes = [
    {
        path: '/login',
        name: 'login',
        component: () => import('../views/base/login/login.vue')
    },
    {
        path: '/test',
        name: 'test',
        component: () => import('../views/test.vue')
    },
    {
        path: '/',
        redirect: '/layout'
    },
    {
        path: '/test',
        name: 'test',
        component: () => import('../views/test.vue')
    },
    {
        path: '/layout',
        name: 'layout',
        component: () => import('../layout/layout.vue'),
        children: [
            {
                path: '/404',
                component: () => import('../views/base/errorPage/404'),
                hidden: true
            },
            {
                path: '/401',
                component: () => import('../views/base/errorPage/401'),
                hidden: true
            },
            {
                path: '/',
                redirect: '/index'
            },
            {
                path: '/redirect/:path*',
                component: () => import('../views/base/redirect')
            },
            {
                path: '/index',
                name: 'index',
                component: () => import('../views/base/index')
            },
            {
                path: '/permission/role',
                name: 'role',
                component: () => import('../views/base/role/role.vue')
            },
            {
                path: '/permission/menus',
                name: 'menus',
                component: () => import('../views/base/menu/menu.vue')
            },
            {
                path: '/permission/user',
                name: 'user',
                component: () => import('../views/base/user/user.vue')
            },
            {
                path: '/system/syslog',
                name: 'syslog',
                component: () => import('../views/base/syslog/syslog.vue')
            },
            {
                path: '/system/quartz',
                name: 'quartz',
                component: () => import('../views/base/quartz/quartz.vue')
            },
        ]
    }
];

let index = new Router({
    scrollBehavior: () => ({y: 0}),
    routes: routes,
    mode: 'history',
});

// 简单配置
NProgress.inc(0.2);
NProgress.configure({easing: 'ease', speed: 500, showSpinner: false});

index.beforeEach((to, from, next) => {
    NProgress.start();
    next()
});

index.afterEach(() => {
    setTimeout(() => {
        NProgress.done()
    }, 200)
});


export default index;
