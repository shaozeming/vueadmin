
import devConfig from './devConfig'
import prodConfig from './prodConfig'

const dev= (process.env.NODE_ENV === 'development');
const config=dev?devConfig:prodConfig;

export default config;
