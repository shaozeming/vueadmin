export const gasState=[
    {
        key: "0000",
        value: '正常',
        type:''
    },
    {
        key: "1000",
        value: '一级低压',
        type:'warning'
    },
    {
        key: "2000",
        value: '二级低压',
        type:'warning'
    },
    {
        key: "4000",
        value: '漏气',
        type:'danger'
    },
    {
        key: "0080",
        value: '开户',
        type:''
    },
    {
        key: "0001",
        value: 'Eeprom_Error',
        type:'danger'
    },
    {
        key: "0002",
        value: 'Eeprom_History_Error',
        type:'danger'
    },
    {
        key: "0004",
        value: '磁攻击',
        type:'warning'
    },
    {
        key: "0008",
        value: '强制关阀',
        type:'danger'
    },
    {
        key: "0020",
        value: '到了关阀提示金额',
        type:'warning'
    },
    {
        key: "0040",
        value: '关阀',
        type:'warning'
    },
    {
        key: "8000",
        value: '干簧管坏',
        type:'danger'
    },
];


export const feeType=[
    {
        key: "00",
        value: '后付费'
    },
    {
        key: "01",
        value: '计量'
    },
    {
        key: "02",
        value: '计费'
    },
];

export const gasType=[
    {
        key: "01",
        value: '无线燃气表'
    },
    {
        key: "02",
        value: '无线IC卡燃气表（民用）'
    },
    {
        key: "03",
        value: '无线IC卡燃气表工业大表'
    },
];
