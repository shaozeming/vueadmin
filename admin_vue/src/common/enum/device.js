export const gasType=[
    {
        key: "0000",
        value: '预付费计量'
    },
    {
        key: "0011",
        value: '预付费计价'
    },
    {
        key: "0022",
        value: '后付费计量'
    },
    {
        key: "0033",
        value: '后付费计价'
    },
];