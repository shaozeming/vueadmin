


export const customerState=[
    {
        key: 0,
        value: '未启用'
    },
    {
        key: 1,
        value: '已启用'
    },
    {
        key: 2,
        value: '已停用'
    },
];

export const customerType=[
    {
        key: 0,
        value: '民用'
    },
    {
        key: 1,
        value: '公建'
    },
    {
        key: 2,
        value: '工业'
    },
];


export const payWay=[
    {
        key: 0,
        value: '现金支付'
    },
    {
        key: 1,
        value: '银行代扣'
    },
    {
        key: 2,
        value: '一户通代扣'
    },
];


export  const  bankCheck=[
    {
        key: 0,
        value: '未验证'
    },
    {
        key: 1,
        value: '验证通过'
    },
    {
        key: 2,
        value: '验证不通过'
    },
];