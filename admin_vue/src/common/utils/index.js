
import dateUtils from './dateUtils'
import objUtils from './objUtils'
import crypto from './crypto'
export default{
    install(Vue,options) {
        Vue.prototype.$dateUtils= dateUtils;
        Vue.prototype.$objUtils= objUtils;
        Vue.prototype.$crypto=crypto;
    }
}
