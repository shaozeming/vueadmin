import qs from 'qs';
import axios from 'axios'
import config from '../config'
import encrypt from './utils/crypto'
import Router from '../router/'
import Cookies from 'js-cookie'

import {Message} from "element-ui";

const request=axios.create({
  baseURL: config.apiUrl, // api的base_url
  timeout: 60000, // request timeout
});



request.interceptors.request.use(function (config) {
  if(config.data){
    // config.data.token='A0B8DD1185D403BC2EF46CF49F07CBF5';
    config.headers["token"]=Cookies.get("token");
    if(config.data.crypto){
      delete config.data['crypto'];
      config.data= encrypt.encryptAll(config.data)

      if(config.data.key){
        config.headers["key"]=config.data.key;
        delete config.data['key'];
      }
    }
    config.data=qs.stringify(config.data,{arrayFormat: 'repeat'});
  }
  return config;
}, function (error) {
  return Promise.reject(error);
});


request.interceptors.response.use(function (response) {
  if(response.data){
    response=response.data
  }
  /*尚未登陆*/
  if(response.status===6){
    Router.push({path: '/login'})
  }

  return response
}, function (error) {
  let response=error.response

  if(!response){
    Message.error("网络错误");
  }

  if(response.status===401){
    Message.error("登陆失效");

    setTimeout(()=>{
      Router.push({path: '/login'})
    },2000)
    return null
  }
  if(response.status===403){
    Router.push({path: '/401'})
  }else {
    Message.error("网络错误");
  }
  // Do something with response error
  return Promise.reject(error)


});

export default  request;


const request1=axios.create({
  baseURL: config.apiUrl, // api的base_url
  timeout: 60000, // request timeout
});



request1.interceptors.request.use(function (config) {
  if(config.data){
     // config.data.token='A0B8DD1185D403BC2EF46CF49F07CBF5';
    config.headers["token"]=Cookies.get("token");
    if(config.data.crypto){
      delete config.data['crypto'];
      config.data= encrypt.encryptAll(config.data)

      if(config.data.key){
        config.headers["key"]=config.data.key;
        delete config.data['key'];
      }
    }
    config.data=qs.stringify(config.data,{arrayFormat: 'repeat'});
  }
  return config;
}, function (error) {
  return Promise.reject(error);
});


request1.interceptors.response.use(function (response) {
  if(response.data){
    response=response.data
  }
  /*尚未登陆*/
  if(response.status===6){
    Router.push({path: '/login'})
  }

  return response
}, function (error) {
  let response=error.response

  if(!response){
    Message.error("网络错误");
  }

  if(response.status===401){
    Message.error("登陆失效");

    setTimeout(()=>{
      Router.push({path: '/login'})
    },2000)
    return null
  }
  if(response.status===403){
     Router.push({path: '/401'})
  }else {
    Message.error("网络错误");
  }
  // Do something with response error
  return Promise.reject(error)


});

export const fromRequest=request1;














const request2=axios.create({
  baseURL: config.apiUrl, // api的base_url
  timeout: 60000, // request timeout
  headers:{'Content-Type':'multipart/form-data'}
});
request2.interceptors.request.use(function (config) {
  if(config.data){
    // config.data.token='A0B8DD1185D403BC2EF46CF49F07CBF5';
     config.data.append("token",Cookies.get("token"));
  }
  return config;
}, function (error) {
  return Promise.reject(error);
});
export const requestMultipart=request2;



const request3=axios.create({
  baseURL: config.apiUrl, // api的base_url
  timeout: 600000, // request timeout
});

request3.interceptors.request.use(function (config) {
  if(config.data){
    // config.data.token='A0B8DD1185D403BC2EF46CF49F07CBF5';
    config.data.token=Cookies.get("token");
    if(!config.data.token)  config.data.token=Cookies.get("token");
    if(config.data.crypto){
      delete config.data['crypto'];
      config.data= encrypt.encryptAll(config.data)
    }
    config.data=qs.stringify(config.data,{arrayFormat: 'repeat'});
  }
  return config;
}, function (error) {
  return Promise.reject(error);
});

request3.interceptors.response.use(function (response) {
  if(response.headers && response.data&&response.data.size) {
    let blob = new Blob([response.data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8'});
    let downloadElement = document.createElement('a');
    let href = window.URL.createObjectURL(blob); //创建下载的链接
    downloadElement.href = href;
    downloadElement.download =response.config.filename+ '.xlsx'; //下载后文件名
    document.body.appendChild(downloadElement);
    downloadElement.click(); //点击下载
    document.body.removeChild(downloadElement); //下载完成移除元素
    window.URL.revokeObjectURL(href); //释放掉blob对象

  }
  return response
}, function (error) {
  // Do something with response error
  return Promise.reject(error)
});

export const requestExport=request3;
