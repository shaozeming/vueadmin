
export  default {
    data(){
        return{
            tableData:[],
            ids:[],
            loading:false,
            pageTotal:0,
            searchParam:{},
            filterParams:{},
            pageParam:{
                page:1,
                size:10,
                sortParam:"",
                direction:""
            }

        }
    },
    created() {
        if(!this.noCurdInit){
            this.initList();
        }

    },
    methods: {
        initList(){
            this.pageParam.page=1;
            if(this.preList){
                this.preList();
            }
            if(this.customerInit){
                this.customerInit();
            }
            this.list()
        },

        search(){
            this.pageParam.page=1;
            if(this.searchInit){
                this.searchInit();
            }
            this.list()
        },

        curChange(page){
            this.pageParam.page=page;
            this.list()
        },
        refresh(){
            if(this.customerInit){
                this.customerInit();
            }
            this.list()
        },
        list(){
            this.loading=true;
            if(!this.getList){
                return;
            }
            this.$nextTick(()=>{
                let param=this.getParam();
                this.getList(param).then((res)=>{
                    if(res.status===0){
                        this.tableData=res.data.records;
                        if(res.data.total){
                            this.pageTotal=res.data.total;
                        }
                    }else {
                        this.$message.error(res.msg)
                    }
                    if(this.listBack){
                        this.listBack();
                    }
                    this.loading=false;
                }).catch(res=>{
                    this.loading=false;
                })
            })
        },

        getParam(){

            return {
                ...this.pageParam,
                param:JSON.stringify({...this.searchParam, ...this.filterParams})
            };
        },
        add(){
            this.$refs.save.setData();
        },

        handleEdit(data){
            this.$refs.save.setData(JSON.parse(JSON.stringify(data)));
        },
        clearSearch(){
            if(this.clearSearchBack){
                this.clearSearchBack();
            }
            this.searchParam={};
            this.search()
        },
        handleDelete(data) {
            if(data){
                this.$confirm('确定删除, 是否继续?', '提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning'
                }).then(() => {
                    if(this.delete){
                        this.delete(data).then(res=>{
                            if(res.status===0){
                                this.$notify({
                                    title: '删除成功',
                                    type: 'success',
                                    duration: 1500
                                });
                                this.refresh();
                            }else {
                                this.$message.error(res.msg);
                            }
                        })
                    }


                }).catch(() => {

                });
            }
        },
        deleteAll(){
            if(this.ids.length>0){
                this.$confirm('确定批量删除, 是否继续?', '提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning'
                }).then(() => {
                    if(this.deleteBatch){
                        this.deleteBatch({ids:this.ids}).then(res=>{
                            if(res.status===0){
                                this.$notify({
                                    title: '批量删除成功',
                                    type: 'success',
                                    duration: 1500
                                });
                                this.refresh();
                            }else {
                                this.$message.error(res.msg);
                            }
                        })
                    }


                }).catch(() => {

                });
            }
        },
        sortChange(data){
            if(data.order){
                let prop=data.prop;
                let pre="";
                if(prop.indexOf(".")>0){
                    pre= prop.substring(0,prop.indexOf("."));
                    prop=prop.substring(prop.indexOf("."),prop.length);
                }
                prop=this.$objUtils.objectTotable(prop);
                if(pre){
                    prop=pre+prop;
                }
                this.pageParam.sortParam=prop;
                this.pageParam.direction=(data.order.startsWith("desc")?"0":"1");

            }else {
                this.pageParam.sortParam="";
                this.pageParam.direction="";
                this.$refs.table.clearSort();
            }
            this.list()

        },
        selectChange(selection){
            let ids=[];
            selection.forEach(item=>{
                ids.push(item.id);
            });
            this.ids=ids;
        },
        filterHandler(value, row, column) {
            return true;
        },
        filterChange(filter){
            Object.keys(filter).forEach(item=>{
                if(filter[item] &&filter[item].length>0){
                    this.filterParams[item]=filter[item].join("||");
                }else {
                    delete  this.filterParams[item]
                }
            });
            this.list()
        },
        exports(){
            return this.export().then(res=>{
                console.log(res)
            });
        },
        sizeChange(data){
            this.pageParam.page=1;
            this.pageParam.size=data;
            this.refresh()
        },
        tableCenter(data){
            return "table_center";
        },
        tableLeft(data){
            return "table_left";
        },
    }
}
