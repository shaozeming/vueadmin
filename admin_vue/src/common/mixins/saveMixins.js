export default {
    data() {
        return {
            title:'',
            editForm: {},
            formLabelWidth: '120px',
            dialogVisible: false,
            isloading: false,
            fullscreen: false,
        }
    },
    created() {

    },
    methods: {
        setData(data) {
            if (this.defaultVal) {
                this.$objUtils.initObjDefault(this.editForm, this.defaultVal)
            } else {
                this.editForm={}
            }
            this.$nextTick(()=>{
                if (this.$refs["save"]) {
                    this.$refs["save"].clearValidate();
                }
            });
            if (data && typeof data === 'object') {
                this.editForm = data;
            }
            if(this.editForm.id){
                this.title=this.module+"编辑"
            }else {
                this.title=this.module+"新增"
            }

            this.dialogVisible = true;
            if (this.setDataBack) {
                this.setDataBack(data);
            }

        },
        save() {
            this.isloading = true;
            if (this.saveBack) {
                this.saveBack();
            }
            let saveFunc = () => {
                let commitFunc=this.commit(this.editForm);
                if(!commitFunc){
                    this.isloading = false;
                    return
                }
                commitFunc.then((res) => {
                    if (res.status === 0) {
                        this.$notify({
                            title: '保存成功',
                            type: 'success',
                            duration: 1500
                        });
                        this.$emit("refresh")
                        this.dialogVisible = false;
                    } else {
                        this.$message.error(res.msg);
                    }
                    this.isloading = false;

                }).catch(res => {
                    this.isloading = false;

                })
            };
            if (this.$refs["save"]) {
                this.$refs["save"].validate((valid) => {
                    if (valid) {
                        saveFunc()
                    }else {
                        this.isloading = false;
                    }
                })
            } else {
                saveFunc()
            }
        },
    }
}
