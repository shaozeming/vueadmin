import Vue from 'vue'

/*文本框的特效指令*/
Vue.directive('inputfocus', {
  // 第一次绑定到元素时
  bind: function (el, binding, vnode) {

    el.style.position='relative';
    //添加下边框
    let border= document.createElement("div");
    border.style.position='absolute';
    border.style.bottom='-1px';
    border.style.left='50%';
    border.style.width='0px';
    border.style.transition='all 0.3s ease-out';
    border.style.height='2px';
    border.style.backgroundColor='#999';
    border.className='focus_border';
    el.appendChild(border);

    //给输入框添加事件
    let input=  el.querySelector('input');

    input.addEventListener('focus',()=>{

      border.style.left='0';
      border.style.width='100%';
    });

    input.addEventListener('blur',function () {

      let border=el.querySelector('.focus_border');
      if(this.value){
        border.style.left='0';
        border.style.width='100%';
      }else {
        border.style.left='50%';
        border.style.width='0';
      }
    });
  }
});

