import Vue from 'vue'
import store from '../../store'
/*文本框的特效指令*/
Vue.directive('permission', {
    // 第一次绑定到元素时
    componentUpdated: function (el, binding, vnode) {

        let aa=2;
        let routes=store.state.sidebar.routes;
        let permission=  findPermission(binding,routes);
        if(!permission){
            el.style.display="none"
        }

    }
});



function findPermission(binding,routes) {
    let permission=false;
    if(binding.value){
        if(routes&&routes.length>0){
            routes.forEach(item=>{
                if(item.permsCode===binding.value){
                    permission=true;
                }
                if(item.children&&item.children.length>0){
                    if(findPermission(binding,item.children)){
                        permission=true;
                    }
                }
            })
        }
    }
    return permission;
}

