export default {
    roleName: [
        {validator: validateRequired,required:true, trigger: 'blur'},
    ],

};



function validateRequired(rule, value, callback) {
   if(value&&typeof value==='string'){
       value=value.trim();
   }
    if (!value) {
        callback(new Error('请输入角色名'));
    } else {
        callback();
    }
}
