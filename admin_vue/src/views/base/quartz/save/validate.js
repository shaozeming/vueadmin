export default {
    quartzName: [
        {validator: validateEmpty,required:true, message: '请输入定时器名称', trigger: 'blur'},
    ],
    cron: [
        {validator: validateEmpty, required:true, message: '请输入cron表达式', trigger: 'blur'},
    ],
};
function validateLength(rule, value, callback) {
    if(value&&typeof value==='string'){
        value=value.trim();
    }
    if(!value){
        callback(new Error(rule.message));
    }
    if(value&&rule.min&&value.length<rule.min){
        callback(new Error(rule.message));
    }
    if(value&&rule.max&&value.length>rule.min){
        callback(new Error(rule.message));
    }
    callback();
}
function validateEmpty(rule, value, callback) {
    if(value&&typeof value==='string'){
        value=value.trim();
    }
    if(!value){
        callback(new Error(rule.message));
    }
    callback();
}

