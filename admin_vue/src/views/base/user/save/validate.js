export default {
    nickName: [
        {validator: validateEmpty,required:true, message: '请输入用户名', trigger: 'blur'},
    ],
    username: [
        {validator: validateEmpty, required:true, message: '请输入账号', trigger: 'blur'},
        {min: 3,validator: validateLength, message: '账号不能小于3位', trigger: 'blur'},
    ],
    password: [
        {validator: validateEmpty,required:true, message: '请输入密码', trigger: 'blur'},
        {min: 6,validator: validateLength, message: '密码不能少于6位', trigger: 'blur'},
    ]
};
function validateLength(rule, value, callback) {
    if(value&&typeof value==='string'){
        value=value.trim();
    }
    if(!value){
        callback(new Error(rule.message));
    }
    if(value&&rule.min&&value.length<rule.min){
        callback(new Error(rule.message));
    }
    if(value&&rule.max&&value.length>rule.min){
        callback(new Error(rule.message));
    }
    callback();
}
function validateEmpty(rule, value, callback) {
    if(value&&typeof value==='string'){
        value=value.trim();
    }
    if(!value){
        callback(new Error(rule.message));
    }
    callback();
}

