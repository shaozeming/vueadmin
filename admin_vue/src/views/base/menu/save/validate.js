export default {
    menuName: [
        {validator: validateEmpty,required: true, message: '请输入名称', trigger: 'blur'},
    ],
    parentId: [
        {validator: validateEmpty,required: true, message: '请选择上级菜单', trigger: 'change'},
    ],
    routerUrl: [
        {validator: validateEmpty,required: true, message: '请输入路由地址', trigger: 'blur'},
    ],
    componentName: [
        {validator: validateEmpty,required: true, message: '请输入组件', trigger: 'blur'},
    ],
    menuIcon: [
        {validator: validateEmpty,required: true, message: '请输入菜单图标', trigger: 'blur'},
    ],
};
function validateLength(rule, value, callback) {
    if(value&&typeof value==='string'){
        value=value.trim();
    }
    if(!value){
        callback(new Error(rule.message));
    }
    if(value&&rule.min&&value.length<rule.min){
        callback(new Error(rule.message));
    }
    if(value&&rule.max&&value.length>rule.min){
        callback(new Error(rule.message));
    }
    callback();
}
function validateEmpty(rule, value, callback) {
    if(value&&typeof value==='string'){
        value=value.trim();
    }
    if(!value){
        callback(new Error(rule.message));
    }
    callback();
}

