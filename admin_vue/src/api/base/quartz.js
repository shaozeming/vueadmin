import request from '../../common/request'

const base="adminApi/quartz";

export  function updateQuartzState(param) {
    return request({
        url: base+'/updateQuartzState',
        method: 'post',
        data: {...param, crypto:true}
    })
}

export  function selectQuartzList(param) {
    return request({
        url: base+'/selectQuartzList',
        method: 'post',
        data: {...param, crypto:true}
    })
}

export  function selectById(param) {
    return request({
        url: base+'/selectById',
        method: 'post',
        data: {...param, crypto:true}
    })
}

export  function saveQuartz(param) {
    return request({
        url: base+'/saveQuartz',
        method: 'post',
        data: {...param, crypto:true}
    })
}
export  function deleteById(param) {
    return request({
        url: base+'/deleteById',
        method: 'post',
        data: {...param, crypto:true}
    })
}



