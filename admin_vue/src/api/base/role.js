
import request from '../../common/request'

const base="adminApi/role";


export  function selectPage(param) {
    return request({
        url: base+'/selectPage',
        method: 'post',
        data: {...param, crypto:true}
    })
}


export  function selectById(param) {
    return request({
        url: base+'/selectById',
        method: 'post',
        data: {...param, crypto:true}
    })
}

export  function save(param) {
    return request({
        url: base+'/save',
        method: 'post',
        data: {...param, crypto:true}
    })
}
export  function deleteById(param) {
    return request({
        url: base+'/deleteById',
        method: 'post',
        data: {...param, crypto:true}
    })
}
export  function saveRoleAuth(param) {
    return request({
        url: base+'/saveRoleAuth',
        method: 'post',
        data: {...param, crypto:true}
    })
}
export  function selectAll(param) {
    return request({
        url: base+'/selectAll',
        method: 'post',
        data: {...param, crypto:true}
    })
}

export  function selectAuthority(param) {
    return request({
        url: base+'/selectAuthority',
        method: 'post',
        data: {...param, crypto:true}
    })
}
