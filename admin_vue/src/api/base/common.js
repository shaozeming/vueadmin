
import {fromRequest, requestMultipart} from '../../common/request'
const base="common";


export  function upload(param) {
    return requestMultipart({
        url: base+'/upload',
        method: 'post',
        data: param
    })
}
export  function selectMenuListByUser(param) {
    return fromRequest({
        url:  base+'/selectMenuListByUser',
        method: 'post',
        data: {...param, crypto:true}
    })
}
export  function login(param) {
    return fromRequest({
        url:  base+'/login',
        method: 'post',
        data: {...param, crypto:true}
    })
}
