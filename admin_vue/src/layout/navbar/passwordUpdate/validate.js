export default {
    newPassword: [
        {validator: validateEmpty, message: '请输入新密码', trigger: 'blur'},
        {min: 6,validator: validateLength, message: '密码不能少于6位', trigger: 'blur'},
    ],
    oldPassword: [
        {validator: validateEmpty, message: '请输入旧密码', trigger: 'blur'},
        {min: 6,validator: validateLength, message: '密码不能少于6位', trigger: 'blur'},
    ]
};
function validateLength(rule, value, callback) {
    if(value){
        value=value.trim();
    }
    if(!value){
        callback(new Error(rule.message));
    }
    if(value&&rule.min&&value.length<rule.min){
        callback(new Error(rule.message));
    }
    if(value&&rule.max&&value.length>rule.min){
        callback(new Error(rule.message));
    }
    callback();
}
function validateEmpty(rule, value, callback) {
    if(value){
        value=value.trim();
    }
    if(!value){
        callback(new Error(rule.message));
    }
    callback();
}

