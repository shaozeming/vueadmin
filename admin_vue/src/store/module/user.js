
import Cookies from 'js-cookie'
const tagview={
    state:{
        avatar: Cookies.get("avatar")? Cookies.get("avatar"):"/img/avatar.png",
        token: Cookies.get("token"),
        nickname: "成员"
    },
    mutations: {


    },
    actions: {
        Login({ dispatch,commit, state },user){

              Cookies.set("user",user);
              if(user&&user.token){
                  Cookies.set("token",user.token);
              }
              if(user&&user.nickName){
                  state.nickname=user.nickName;
                  Cookies.set("nickName",user.nickName);
              }
              if(user&&user.avatar){
                  Cookies.set("avatar",user.avatar);
              }
            dispatch("delAllViews");

        },
        Logout({ commit, state }) {
            Cookies.remove("user")
            Cookies.remove("avatar")
            Cookies.remove("token")
            Cookies.remove("nickName")
        }
    }

};
export default tagview;
