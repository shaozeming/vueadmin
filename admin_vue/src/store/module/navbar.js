
import Cookies from 'js-cookie'
import Vue from 'vue'

const navbar={
    state:{
         backlog:{withdraw:0,authentication:0},
         notify:{},
    },
    mutations: {
        ADD_BACKLOG: (state,data)=>{
           if(data.component==='withdraw'){
               state.backlog.withdraw=state.backlog.withdraw+data.num;
           }else if(data.component==='authentication'){
               state.backlog.authentication=state.backlog.authentication+data.num;
           }
        },

        DELETE_BACKLOG: (state,component)=>{
            if(component==='withdraw'){
                if(state.backlog.withdraw>0){
                    state.backlog.withdraw=state.backlog.withdraw-1;
                }

            }else if(component==='authentication'){
                if(state.backlog.authentication>0){
                    state.backlog.authentication=state.backlog.authentication-1;
                }
            }
        },

    },
    actions:{
        addBacklog({commit},data){
            commit('ADD_BACKLOG',data);
        },
        deleteBacklog({commit},component){
            commit('DELETE_BACKLOG',component);
        },
    }

};
export default navbar;
