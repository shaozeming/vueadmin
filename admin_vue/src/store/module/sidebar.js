
import Cookies from 'js-cookie'
import Vue from 'vue'
const sidebar={
    state:{
        opened: Cookies.get('sidebarStatus') ? !!+Cookies.get('sidebarStatus') : true,
        affix:[
            {
                id:0, menuName:'首页',routerUrl:'/index',menuIcon:'fa-book',children:null,affix:true,hidden:true
            },
        ],
        routes:[
            {
                id:0, menuName:'首页',routerUrl:'/index',menuIcon:'fa-book',children:null,affix:true,hidden:true
            },

        ],

    },
    mutations: {
        TOGGLE_SIDEBAR: state => {
            state.opened = !state.opened
            state.withoutAnimation = false
            if (state.opened) {
                Cookies.set('sidebarStatus', 1)
            } else {
                Cookies.set('sidebarStatus', 0)
            }
        },
        ADD_ROUTES: (state,routes)=>{
            state.routes=state.affix.concat(routes);
        },
        ADD_UNREAD: (state,data)=>{
            state.routes.forEach((item,index)=>{
                 if(item.componentName===data.name){
                     item.unread=data.num;
                 }
                 if(item.children&&item.children.length>0){
                     item.children.forEach(child=>{
                         if(child.componentName===data.name){
                             child.unread=data.num;
                             item.unread=data.num;
                             Vue.set(state.routes, index, JSON.parse(JSON.stringify(item)))
                         }
                     })
                 }
            })


        }
    },
    actions:{
        toggleSideBar({ commit }) {
            commit('TOGGLE_SIDEBAR')
        },
        addRoutes({commit},routes){
            commit('ADD_ROUTES',routes);
        },
        addUnRead({commit},data){
            commit('ADD_UNREAD',data);
        }
    }

};
export default sidebar;
