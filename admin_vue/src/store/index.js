import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import mutations from './mutations'
import getters from './getter'
import sidebar from './module/sidebar'
import tagview from './module/tagview'
import  user from './module/user'
import navbar from './module/navbar'

Vue.use(Vuex);

export default new Vuex.Store({
    modules:{
        sidebar,
        tagview,
        user,
        navbar,
    },
    getters,
    actions,
    mutations
})
