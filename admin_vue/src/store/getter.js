import navbar from "./module/navbar";

const getters = {
    opened: state => state.sidebar.opened,
    routes: state => state.sidebar.routes,
    paths: state => state.sidebar.paths,
    visitedViews: state => state.tagview.visitedViews,
    cachedViews: state => state.tagview.cachedViews,
    avatar: state => state.user.avatar,
    token: state => state.user.token,
    nickname: state => state.user.nickname,
    backlog: state => state.navbar.backlog,
    notify: state => state.navbar.notify,
};
export default getters
