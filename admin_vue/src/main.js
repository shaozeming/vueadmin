import Vue from 'vue'
import App from './App.vue'
import 'normalize.css/normalize.css'
import './styles/reset.css'
import router from './router'
import store from './store/index'
import './element-variables.scss';
import ElementUI from 'element-ui';
import utils from './common/utils/index'
import 'font-awesome/css/font-awesome.css'
import './common/filter'
import './common/directives'
import VueBus from 'vue-bus'




Vue.config.productionTip = false;
Vue.use(VueBus);
Vue.use(utils);
Vue.use(ElementUI);
new Vue({
  router: router,
  store,
  render: h => h(App)
}).$mount('#app');
