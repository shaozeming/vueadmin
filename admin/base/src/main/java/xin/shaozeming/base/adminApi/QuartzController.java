package xin.shaozeming.base.adminApi;


import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import xin.shaozeming.base.common.BaseController;
import xin.shaozeming.base.common.Enum.base.LoggerType;
import xin.shaozeming.base.common.Enum.base.State;
import xin.shaozeming.base.common.Response;
import xin.shaozeming.base.common.aop.annotation.ParamsDecrypted;
import xin.shaozeming.base.common.aop.annotation.Permission;
import xin.shaozeming.base.common.aop.annotation.SysLogger;
import xin.shaozeming.base.common.aop.annotation.TokenCheck;
import xin.shaozeming.base.po.QuartzPO;
import xin.shaozeming.base.service.QuartzService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-17
 */

@Permission(code = "quartz:list")
@SysLogger(group = "定时器")
@RestController
@RequestMapping("adminApi/quartz")
public class QuartzController extends BaseController<QuartzService,QuartzPO> {

    @Resource
    private  QuartzService quartzService;


    @SysLogger(name = "新增更新",type = LoggerType.SAVE,entity = 2)
    @Permission(code = "add,update")
    @TokenCheck
    @ParamsDecrypted
    @ApiOperation(value = "保存或更新定时器", notes = "保存或更新", response = Response.class)
    @PostMapping("saveQuartz")
    public Response saveQuartz(HttpServletRequest request,
                               QuartzPO quartzPO) {

        if(StringUtils.isEmpty(quartzPO)){
            return  new Response(State.RES_PARAMERROR.getCode());
        }
        if (quartzService.saveQuartz(quartzPO)) {
            return new Response<>(State.RES_SUCCES.getCode());
        }
        return new Response<>(State.RES_ERROR.getCode());
    }


    @SysLogger(name = "列表查询",type = LoggerType.SELECT)
    @TokenCheck
    @ParamsDecrypted
    @ApiOperation(value = "查询定时器列表", notes = "查询定时器列表", response = Response.class)
    @PostMapping("selectQuartzList")
    public Response selectQuartzList(HttpServletRequest request) {

        return new Response<>(quartzService.list());
    }

    @SysLogger(name = "状态更新",type = LoggerType.UPDATE)
    @TokenCheck
    @ParamsDecrypted
    @ApiOperation(value = "更新定时器状态", notes = "更新定时器状态", response = Response.class)
    @PostMapping("updateQuartzState")
    public Response updateQuartzState(HttpServletRequest request, QuartzPO quartzPO) {
        if(StringUtils.isEmpty(quartzPO)){
            return  new Response(State.RES_PARAMERROR.getCode());
        }
        if (quartzService.updateQuartzState(quartzPO)) {
            return new Response<>(State.RES_SUCCES.getCode());
        }
        return new Response<>(State.RES_ERROR.getCode());
    }

}
