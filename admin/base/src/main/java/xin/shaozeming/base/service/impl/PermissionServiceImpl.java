package xin.shaozeming.base.service.impl;

import org.springframework.transaction.annotation.Transactional;
import xin.shaozeming.base.common.Enum.base.PermissionType;
import xin.shaozeming.base.common.utils.ObjConverter;
import xin.shaozeming.base.po.PermissionPO;
import xin.shaozeming.base.dao.PermissionDao;
import xin.shaozeming.base.service.PermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xin.shaozeming.base.vo.PermissionVO;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-12
 */
@Transactional
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionDao, PermissionPO> implements PermissionService {

    @Resource
    private  PermissionDao permissionDao;

    /**
     * 查询用户具有权限的目录
     *
     * @param userId
     * @return
     */
    @Override
    public List<PermissionVO> selectMenuListByUser(Integer userId) {

        List<PermissionPO> permissionList = permissionDao.selectMenuListByUser(userId);

        /*筛选出根目录*/
        List<PermissionVO> rootList=filterPermissionRoot(permissionList);

        filterPermissionVO(rootList,permissionList);

        return rootList;
    }


    /*筛选出根目录*/
    List<PermissionVO>  filterPermissionRoot(List<PermissionPO> list){
        List<PermissionPO> rootList=list.stream().filter(item->{
          return   PermissionType.ROOT.getCode().equals(item.getType());
        }).collect(Collectors.toList());
        return rootList.stream().map(item->{
            PermissionVO permissionVO = ObjConverter.convert(item, PermissionVO.class);
            permissionVO.setChildren(new ArrayList<>());
            return  permissionVO;
        }).collect(Collectors.toList());
    }



    /*递归筛选出子目录*/
   void   filterPermissionVO(List<PermissionVO> parentList,List<PermissionPO> list){
       List<PermissionVO> childList=new ArrayList<>();

       /*先筛选出当前目录列表的所有子目录*/
       parentList.forEach(parent->{
           list.forEach(child->{
                  if(parent.getId().equals(child.getParentId())){
                      PermissionVO permissionVO = ObjConverter.convert(child, PermissionVO.class);
                      permissionVO.setChildren(new ArrayList<>());
                      childList.add(permissionVO);
                  }
              });
       });

       /*再筛选出所有子目录的下一级子目录*/
       if(childList.size()>0){
           filterPermissionVO(childList,list);
       }

       /*将子目录对应上一级目录*/
       parentList.forEach(parent->{
           childList.forEach(child->{
               if(parent.getId().equals(child.getParentId())){
                   parent.getChildren().add(child);
               }
           });
       });

    }


    /**
     * 查询所有的目录
     *
     * @return
     */
    @Override
    public List<PermissionVO> selectMenuList() {
        List<PermissionPO> permissionList = list();
        /*筛选出根目录*/
        List<PermissionVO> rootList=filterPermissionRoot(permissionList);

        filterPermissionVO(rootList,permissionList);

        return rootList;
    }

    /**
     * 查询角色目录列表
     *
     * @param roleId
     * @return
     */
    @Override
    public List<PermissionPO> selectPermissionByRole(Integer roleId) {
        return permissionDao.selectPermissionByRole(roleId);
    }

    /**
     * 检查角色是否具有权限
     *
     * @param token
     * @param list
     * @return
     */
    @Override
    public int checkUserPermission(String token, List<String> list) {
        return permissionDao.checkUserPermission(token,list);
    }
}
