package xin.shaozeming.base.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: 邵泽铭
 * @date: 2019/5/10
 * @description:
 **/
@Data
public class RoleUserDTO implements Serializable {
    private static final long serialVersionUID = 7504378905020276889L;
    private Integer roleId;
    private String roleName;
    private Integer userId;
}
