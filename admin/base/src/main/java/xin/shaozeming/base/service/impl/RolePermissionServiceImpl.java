package xin.shaozeming.base.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.transaction.annotation.Transactional;
import xin.shaozeming.base.common.ex.CustomerException;
import xin.shaozeming.base.po.RolePermissionPO;
import xin.shaozeming.base.dao.RolePermissionDao;
import xin.shaozeming.base.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-12
 */
@Transactional
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionDao, RolePermissionPO> implements RolePermissionService {

    /**
     * 更新角色权限
     *
     * @param roleId
     * @param ids
     * @return
     */
    @Override
    public boolean updateRolePermissionByRole(Integer roleId, String ids) {

        List<RolePermissionPO> list=new ArrayList<>();

        JSONArray array= JSONObject.parseArray(ids);
        for (int i=0;i<array.size();i++){
            RolePermissionPO rolePermissionPO=new RolePermissionPO();
            Integer menuId=  array.getInteger(i);
            rolePermissionPO.setPermissionId(menuId);
            rolePermissionPO.setRoleId(roleId);
            list.add(rolePermissionPO);
        }

        lambdaUpdate().eq(RolePermissionPO::getRoleId,roleId ).remove();

        if(!saveBatch(list)){
            throw new CustomerException("角色更新权限失败");
        }


        return true;
    }
}
