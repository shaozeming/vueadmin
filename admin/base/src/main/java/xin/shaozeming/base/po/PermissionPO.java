package xin.shaozeming.base.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_permission")
@ApiModel(value="PermissionPO对象", description="")
public class PermissionPO extends Model<PermissionPO> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "父id")
    private Integer parentId;


    @ApiModelProperty(value = "目录名称")
    private String menuName;

    @ApiModelProperty(value = "前端路由地址")
    private String routerUrl;

    @ApiModelProperty(value = "目录图标")
    private String menuIcon;

    @ApiModelProperty(value = "前端组件名称")
    private String componentName;

    @ApiModelProperty(value = "排序号")
    private Integer priority;

    @ApiModelProperty(value = "0 目录 1 菜单 2 按钮/权限")
    private Integer type;

    @ApiModelProperty(value = "权限编号")
    private String permsCode;

    @ApiModelProperty(value = "创建时间")
    private Date createdTime;

    @ApiModelProperty(value = "修改时间")
    private Date modifiedTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
