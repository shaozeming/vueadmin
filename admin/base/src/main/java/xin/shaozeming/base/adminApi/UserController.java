package xin.shaozeming.base.adminApi;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import xin.shaozeming.base.common.BaseController;
import xin.shaozeming.base.common.Enum.base.LoggerType;
import xin.shaozeming.base.common.Enum.base.State;
import xin.shaozeming.base.common.Response;
import xin.shaozeming.base.common.aop.annotation.ParamsDecrypted;
import xin.shaozeming.base.common.aop.annotation.Permission;
import xin.shaozeming.base.common.aop.annotation.SysLogger;
import xin.shaozeming.base.common.aop.annotation.TokenCheck;
import xin.shaozeming.base.common.utils.ObjConverter;
import xin.shaozeming.base.common.utils.TokenUtils;
import xin.shaozeming.base.po.UserPO;
import xin.shaozeming.base.po.UserRolePO;
import xin.shaozeming.base.service.RoleService;
import xin.shaozeming.base.service.UserRoleService;
import xin.shaozeming.base.service.UserService;
import xin.shaozeming.base.vo.UserRoleVO;
import xin.shaozeming.base.vo.UserVO;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-08-22
 */
@SysLogger(group = "用户")
@Permission(code = "user:list")
@RestController
@RequestMapping("adminApi/user")
public class UserController extends BaseController<UserService,UserPO> {
    @Resource
    private UserRoleService userRoleService;
    @Resource
    private UserService userService;

    @SysLogger(name = "列表查询",type = LoggerType.SELECT)
    @ParamsDecrypted
    @TokenCheck
    @ApiOperation(value = "基础分页加强", notes = "基础分页加强", response = Response.class)
    @PostMapping("selectPageOver")
    public Response selectPageOver( HttpServletRequest request,
                                   @ApiParam(name = "page" ,value = "页码") String page ,
                                   @ApiParam(name = "size" ,value = "数量") String size,
                                   @ApiParam(name = "sortParam" ,value = "排序字段") String sortParam,
                                   @ApiParam(name = "direction" ,value = "排序",defaultValue = "0") String direction,
                                   @ApiParam(name = "param" ,value = "实体json") String param){

        Response<IPage<UserPO>> response= this.selectPage(request,page,size,sortParam,direction,param);
        List<UserPO> userList= response.getData().getRecords();
        List<UserRoleVO> list= userRoleService.selectRoleUserIn(userList);
        return new Response<>( new Page<UserRoleVO>(response.getData().getCurrent(),response.getData().getSize(),response.getData().getTotal()).setRecords(list));
    }

    @SysLogger(name = "修改密码",type = LoggerType.UPDATE)
    @TokenCheck
    @ParamsDecrypted
    @ApiOperation(value = "修改密码", notes = "修改密码", response = Response.class)
    @PostMapping("updatePassword")
    public Response updatePassword(HttpServletRequest request,
                                    @ApiParam(name = "oldPassword" ,value = "oldPassword") String oldPassword,
                                   @ApiParam(name = "newPassword" ,value = "newPassword") String newPassword){

        if(StringUtils.isEmpty(newPassword)||StringUtils.isEmpty(oldPassword)){
            return new Response<>(State.RES_PARAMERROR.getCode());
        }
        if(userService.updatePassword(TokenUtils.getToken(request),newPassword,oldPassword) ){
            return new Response<>(State.RES_SUCCES.getCode() );
        }else {
            return new Response<>(State.RES_ERROR.getCode() );
        }
    }


    @ParamsDecrypted
    @TokenCheck
    @ApiOperation(value = "查询用户信息", notes = "查询用户信息", response = Response.class)
    @PostMapping("selectUserInfo")
    public Response selectUserInfo(HttpServletRequest request){

        return new Response<>(ObjConverter.convert(userService.checkToken(TokenUtils.getToken(request)),UserVO.class));
    }


    @SysLogger(name = "新增更新",type = LoggerType.SAVE,entity = 2)
    @TokenCheck
    @ParamsDecrypted
    @ApiOperation(value = "保存或更新", notes = "保存或更新", response = Response.class)
    @PostMapping("saveUser")
    public Response saveUser(HttpServletRequest request,
                         UserPO  userPO,String roleIds) {

        if(StringUtils.isEmpty(userPO)){
            return  new Response(State.RES_PARAMERROR.getCode());
        }
        userPO.setToken(null);
        if (userService.saveUser(userPO,roleIds)) {
            return new Response<>(State.RES_SUCCES.getCode());
        }
        return new Response<>(State.RES_ERROR.getCode());
    }


}
