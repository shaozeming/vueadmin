package xin.shaozeming.base.config.swagger;

import com.google.common.base.Predicate;
import io.swagger.annotations.Api;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author: 邵泽铭
 * @date: 2019/2/26
 * @description:
 **/


@Configuration
@EnableSwagger2
public class Swagger2 {

    //API文档服务地址：http://localhost:8585/swagger-ui.html

    @Bean
    public Docket createRestApi() {
        Predicate<RequestHandler> selector=RequestHandlerSelectors.withClassAnnotation(Api.class);

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .forCodeGeneration(false)
                .select()
//                .apis(RequestHandlerSelectors.any())
                .apis(selector)

                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("基本后台管理")
                .description("基本后台管理api接口文档")
                .termsOfServiceUrl("http://example.com/")
                .contact("Base")
                .version("1.0")
                .build();
    }

}