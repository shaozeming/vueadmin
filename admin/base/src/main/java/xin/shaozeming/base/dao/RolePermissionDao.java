package xin.shaozeming.base.dao;

import xin.shaozeming.base.po.RolePermissionPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-12
 */
public interface RolePermissionDao extends BaseMapper<RolePermissionPO> {

}
