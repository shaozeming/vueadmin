package xin.shaozeming.base.common.ex;

/**
 * @author: 邵泽铭
 * @date: 2019/7/19
 * @description:
 **/
public class CustomerException extends  RuntimeException {
    private static final long serialVersionUID = -628213535741239179L;

    public CustomerException(){
        super();
    }

    public CustomerException(String msg){
        super(msg);
    }
}
