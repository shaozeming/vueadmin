package xin.shaozeming.base.adminApi;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import xin.shaozeming.base.common.BaseController;
import xin.shaozeming.base.common.Enum.base.LoggerType;
import xin.shaozeming.base.common.Response;
import xin.shaozeming.base.common.aop.annotation.ParamsDecrypted;
import xin.shaozeming.base.common.aop.annotation.Permission;
import xin.shaozeming.base.common.aop.annotation.SysLogger;
import xin.shaozeming.base.common.aop.annotation.TokenCheck;
import xin.shaozeming.base.common.utils.ParamUtils;
import xin.shaozeming.base.po.PermissionPO;
import xin.shaozeming.base.service.PermissionService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-12
 */
@Permission(code = "menus:list")
@RestController
@SysLogger(group = "目录")
@RequestMapping("adminApi/permission")
public class PermissionController extends BaseController<PermissionService,PermissionPO> {


    @Resource
    private  PermissionService permissionService;



    @Permission(code = "role:list")
    @ParamsDecrypted
    @TokenCheck
    @SysLogger(name = "列表查询",type = LoggerType.SELECT)
    @ApiOperation(value = "查找目录列表", notes = "查找目录列表", response = Response.class)
    @PostMapping("selectMenuList")
    public Response selectMenuList( HttpServletRequest request){

        return new Response<>( permissionService.selectMenuList());
    }
}
