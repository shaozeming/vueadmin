package xin.shaozeming.base.dao;

import org.apache.ibatis.annotations.Param;
import xin.shaozeming.base.po.PermissionPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-12
 */
public interface PermissionDao extends BaseMapper<PermissionPO> {

    /**
     * 根据用户查询目录列表
     * @return
     */
    List<PermissionPO> selectMenuListByUser(@Param("id")Integer id);

    /**
     * 查询角色查询目录列表
     * @return
     */
    List<PermissionPO> selectPermissionByRole(@Param("roleId") Integer roleId);


    /**
     * 检查角色是否具有权限
     * @return
     */
    int checkUserPermission(@Param("token") String token, @Param("list") List<String> list);
}
