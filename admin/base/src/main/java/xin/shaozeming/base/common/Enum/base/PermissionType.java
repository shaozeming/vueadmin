package xin.shaozeming.base.common.Enum.base;

/**
 * @author: 邵泽铭
 * @date: 2019/8/26
 * @description:
 **/
public enum PermissionType {
      ROOT( 0,"一级菜单"),
      CHILDREN( 1,"子菜单"),
     FUNCTION( 2,"按钮功能"),
    ;
    private Integer code;
    private String value;
    private PermissionType(Integer code, String value){
        this.code=code;
        this.value=value;
    }
    public Integer getCode(){
        return this.code;
    }
    public String getValue(){
        return this.value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
