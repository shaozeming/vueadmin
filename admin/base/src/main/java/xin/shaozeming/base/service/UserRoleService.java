package xin.shaozeming.base.service;

import org.springframework.transaction.annotation.Transactional;
import xin.shaozeming.base.po.UserPO;
import xin.shaozeming.base.po.UserRolePO;
import com.baomidou.mybatisplus.extension.service.IService;
import xin.shaozeming.base.vo.UserRoleVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-08-22
 */
public interface UserRoleService extends IService<UserRolePO> {

    /**
     * 查询用户列表的角色
     * @param list
     * @return
     */
    List<UserRoleVO> selectRoleUserIn(List<UserPO> list);


    /**
     * 更新用户角色
     * @return
     */
    boolean updateUserRoleByRole(Integer userId,List<UserRolePO> list);
}
