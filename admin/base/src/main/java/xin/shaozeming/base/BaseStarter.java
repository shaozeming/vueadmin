package xin.shaozeming.base;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author: 邵泽铭
 * @date: 2019/3/30
 * @description:
 **/

@SpringBootApplication
@MapperScan({"xin.shaozeming.base.dao"})
@EnableTransactionManagement
public class BaseStarter {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(BaseStarter.class,args);
    }
}
