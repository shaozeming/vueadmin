package xin.shaozeming.base.common.utils;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class SmsUtils {
    /**
     * @param args
     */
    
    //短信API产品名称
    private static final String product="Dysmsapi";
    //短信API产品域名
    private static final String domain="dysmsapi.aliyuncs.com";
    //自己的accessKeyId
    private static final String accessKeyId = "";
    //自己的accessKeySecret
    private static final String accessKeySecret = "";
    //短信签名
    private static final String signName = "";

    /*发送短信验证码*/
    public static   String sendRegisterSms(String mobile){
        String code = ToolsUtils.generateCode();
        if("OK".equals(sendCode(mobile, "SMS_169555046", "{\"code\":\""+code+"\"}"))){
            return code;
        }
        return "";

    }
    /*发送短信验证码*/
    public static   String sendResetSms(String mobile){
        String code = ToolsUtils.generateCode();
        if("OK".equals(sendCode(mobile, "SMS_169555046", "{\"code\":\""+code+"\"}"))){
            return code;
        }
        return "";

    }

    private static String sendCode(String mobile,String temlate,String param){
        //设置超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
        //初始化ascClient
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId,
                accessKeySecret);
        try {
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        } catch (ClientException e) {
            log.error("短信出错,错误信息：{}",e.getMessage());
        }
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象
        SendSmsRequest request=new SendSmsRequest();
        //使用post提交
        request.setMethod(MethodType.POST);
        //待发送的手机号
        request.setPhoneNumbers(mobile);
        //短信签名
        request.setSignName(signName);
        //短信模板ID
        request.setTemplateCode(temlate);

        request.setTemplateParam(param);
        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        //request.setOutId("1454");
        SendSmsResponse response= null;
        try {
            response = acsClient.getAcsResponse(request);

        } catch (ClientException e) {
            log.error("短信出错,错误信息：{}",e.getMessage());
        }
        if(response==null){
            return "";
        }
        if(response.getCode() != null && response.getCode().equals("OK")) {
            return "OK";
        }else {
            log.error("短信发送错误,错误代码：{},错误信息：{}",response.getCode(),response.getMessage());
            return response.getMessage();
        }
    }


}