package xin.shaozeming.base.service;

import xin.shaozeming.base.po.PermissionPO;
import com.baomidou.mybatisplus.extension.service.IService;
import xin.shaozeming.base.vo.PermissionVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-12
 */
public interface PermissionService extends IService<PermissionPO> {


    /**
     *  查询用户具有权限的目录
     * @return
     */
    List<PermissionVO> selectMenuListByUser(Integer userId);

    /**
     *  查询所有的目录
     * @return
     */
    List<PermissionVO> selectMenuList();

    /**
     *  查询角色目录列表
     * @return
     */
    List<PermissionPO> selectPermissionByRole(Integer roleId);


    /**
     * 检查角色是否具有权限
     * @return
     */
    int checkUserPermission( String token, List<String> list);
}
