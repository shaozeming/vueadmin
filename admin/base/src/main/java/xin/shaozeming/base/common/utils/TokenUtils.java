package xin.shaozeming.base.common.utils;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import xin.shaozeming.base.common.servlet.ParameterRequestWrapper;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: 邵泽铭
 * @date: 2019/10/14
 * @description:
 **/
public class TokenUtils {

    public static String getToken(HttpServletRequest request){
       return request.getHeader("token");
    }


    public static String getToken(){
        ParameterRequestWrapper requestWrapper = (ParameterRequestWrapper) ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        return requestWrapper.getHeader("token");
    }
}
