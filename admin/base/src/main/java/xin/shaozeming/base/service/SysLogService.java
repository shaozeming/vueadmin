package xin.shaozeming.base.service;

import xin.shaozeming.base.po.SysLogPO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-16
 */
public interface SysLogService extends IService<SysLogPO> {

}
