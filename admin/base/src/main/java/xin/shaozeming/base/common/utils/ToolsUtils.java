package xin.shaozeming.base.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * @author: 邵泽铭
 * @date: 2018/10/25
 * @description:
 **/
public class ToolsUtils {

    static Date today = new Date();
    static int orderIndex = 0;

    public static String getUUID() {

        Date n = new Date();
        SimpleDateFormat outFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String currTime = outFormat.format(n);

        if (orderIndex > 0) {
            if (n.getYear() == today.getYear() && n.getMonth() == today.getMonth() && n.getDay() == today.getDay()) {
                orderIndex += 1;
            } else {
                today = n;
                orderIndex = 1;
            }
        } else {
            today = n;
            orderIndex = 1;
        }
        if (orderIndex > 999999) {
            orderIndex = 1;
        }
        String indexString = String.format("%s%06d", currTime, orderIndex);
        return indexString;
    }

    /**
     * 生成6位随机码
     *
     * @return
     */
    public static String generateCode() {
        int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Random rand = new Random();
        for (int i = 10; i > 1; i--) {
            int index = rand.nextInt(i);
            int tmp = array[index];
            array[index] = array[i - 1];
            array[i - 1] = tmp;
        }
        int result = 0;
        for (int i = 0; i < 6; i++)
            result = result * 10 + array[i];
        return String.valueOf(result).length() < 6 ? generateCode() : String.valueOf(result);
    }

}