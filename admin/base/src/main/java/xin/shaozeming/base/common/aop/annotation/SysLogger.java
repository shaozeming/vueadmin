package xin.shaozeming.base.common.aop.annotation;

import xin.shaozeming.base.common.Enum.base.LoggerType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author: 邵泽铭
 * @date: 2019/4/17
 * @description:
 **/

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
public @interface SysLogger {

    String group() default "";

    String name() default "";

    String log() default "";

    int entity() default 0; /*实体参数的位置,用于保存方法*/

    String id() default "id"; /*实体参数的id,用于保存方法*/

    boolean user() default  false; /* 登录注册等用户信息在参数中的方法*/

    int userPosition() default 1; /*用户参数的位置*/

    LoggerType type() default LoggerType.SELECT;
}
