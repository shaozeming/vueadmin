package xin.shaozeming.base.common.utils;

/**
 * @author: 邵泽铭
 * @date: 2019/5/8
 * @description:
 **/
public class DecryptUtil {

    public static String decryptParam(String key,String param){
        try {
            key=  RSAUtils.decryptByPrivateKey(key);
        } catch (Exception e) {
           return  null;
        }
       return AESUtils.decryptData(key,param);
    }
}
