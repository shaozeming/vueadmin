package xin.shaozeming.base.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import xin.shaozeming.base.po.RolePO;

import java.util.Date;
import java.util.List;

/**
 * @author: 邵泽铭
 * @date: 2019/5/10
 * @description:
 **/

@Data
public class UserRoleVO {


    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户code")
    private String code;

    @ApiModelProperty(value = "用户账号")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;


    @ApiModelProperty(value = "用户姓名")
    private String nickName;

    @ApiModelProperty(value = "0 普通账号 1 手机号 ")
    private Integer accountType;

    @ApiModelProperty(value = "用户手机号")
    private String userPhone;

    @ApiModelProperty(value = "0 使用 1 删除 2 停用")
    private Integer state;

    @ApiModelProperty(value = "上一次登陆时间")
    private Date lastLoginTime;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    private Date modifiedTime;


    private   List<RolePO> roles;
}
